package com.example.saventiy_l27_todo

import android.graphics.Color.parseColor

class ColorGenerator {
    private var mColorsCodeList = listOf(
        "#E53935", "#FF5252", "#D81B60", "#8E24AA",
        "#AA00FF", "#5E35B1", "#303F9F", "#1565C0"
    )

    fun getColor() = getCode(mColorsCodeList)

    private fun getCode(colors: List<String>) =
        colors[getRandInt(colors.size)].run(::parseColor)

    private fun getRandInt(max: Int) =
        (0 until max).shuffled().first()
}