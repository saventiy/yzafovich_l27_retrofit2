package com.example.saventiy_l27_todo.data.retrofit.model

import com.squareup.moshi.Json

class DataImpl(
     var userId: Int,
     var id: Int,
     var title: String,
     var completed: Boolean
)
