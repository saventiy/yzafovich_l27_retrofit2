package com.example.saventiy_l27_todo.data.sourceToDo

import com.example.saventiy_l27_todo.data.retrofit.model.DataImpl
import io.reactivex.Single

interface Repository {
    fun todos(): Single<List<DataImpl>>?
}