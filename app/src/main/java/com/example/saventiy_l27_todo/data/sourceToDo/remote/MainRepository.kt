package com.example.saventiy_l27_todo.data.sourceToDo.remote

import com.example.saventiy_l27_todo.data.retrofit.RetrofitService
import com.example.saventiy_l27_todo.data.sourceToDo.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainRepository : Repository {

    private val data by lazy {
        RetrofitService()
            .loadData().getData()
    }

    override fun todos() = data.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}