package com.example.saventiy_l27_todo.data.retrofit

import com.example.saventiy_l27_todo.data.retrofit.model.DataImpl
import io.reactivex.Single
import retrofit2.http.GET

interface GetData {

    @GET("/todos")
    fun getData(): Single<List<DataImpl>>
}