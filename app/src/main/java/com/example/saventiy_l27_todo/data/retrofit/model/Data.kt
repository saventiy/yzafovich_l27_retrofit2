package com.example.saventiy_l27_todo.data.retrofit.model

interface Data {
    var userId: Int
    var id: Int
    var title: String
    var completed: Boolean
}