package com.example.saventiy_l27_todo.ui.viewModel.adapter

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.RecyclerView
import com.example.saventiy_l27_todo.ColorGenerator
import com.example.saventiy_l27_todo.R
import com.example.saventiy_l27_todo.data.retrofit.model.DataImpl
import kotlinx.android.synthetic.main.item_view.view.*

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>()  {

    private var usersList: List<DataImpl> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val drawable = ColorDrawable(ColorGenerator().getColor())
        val icon = RoundedBitmapDrawableFactory.create(
            parent.resources, drawable.toBitmap(100, 100)
        )
        icon.isCircular = true

        val vholder =  ViewHolder(from(parent))
        vholder.imageViewId.setImageDrawable(icon)
        return vholder
    }

    override fun getItemCount(): Int = usersList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


//        holder.id.text = usersList[position].id.toString()
        holder.userId.text = "Assignee: User" + usersList[position].userId.toString()
        holder.title.text = usersList[position].title


        if (usersList[position].completed) {
            holder.completed.setChipBackgroundColorResource(R.color.doneColor)
            holder.completed.setText(R.string.done)
        } else {
            holder.completed.setChipBackgroundColorResource(R.color.toDoColor)
            holder.completed.setText(R.string.toDo)
        }

        holder.completed.setOnClickListener {
            val item = usersList[holder.adapterPosition]

            if(item.completed){
                holder.completed.setText(R.string.toDo)
                holder.completed.setChipBackgroundColorResource(R.color.toDoColor)
            } else{
                holder.completed.setText(R.string.done)
                holder.completed.setChipBackgroundColorResource(R.color.doneColor)
            }
            item.completed = !item.completed
        }
    }

    fun newList(newList: List<DataImpl>) {
        usersList = newList
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
//        val id = itemView.text_view_id
        val userId = itemView.text_view_userId
        val title = itemView.text_view_title
        val imageViewId = itemView.image_view_id
        val completed = itemView.chip_completed
    }

    companion object {
        fun from(parent: ViewGroup) =
            LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
    }
}