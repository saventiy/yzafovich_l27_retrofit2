package com.example.saventiy_l27_todo.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.saventiy_l27_todo.R
import com.example.saventiy_l27_todo.ui.viewModel.adapter.Adapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: Adapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = Adapter()
        recycler_view_data.layoutManager = LinearLayoutManager(this)
        recycler_view_data.adapter = adapter

        viewModel = MainViewModel()
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.liveData.observe(this, Observer {
            adapter.newList(it)
            adapter.notifyDataSetChanged()
        })
    }
}
