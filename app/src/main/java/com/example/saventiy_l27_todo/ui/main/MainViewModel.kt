package com.example.saventiy_l27_todo.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.saventiy_l27_todo.data.retrofit.model.DataImpl
import com.example.saventiy_l27_todo.data.sourceToDo.remote.MainRepository
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class MainViewModel : ViewModel() {

    val liveData = MutableLiveData<List<DataImpl>>()
    private val mainRepository = MainRepository()

    init {
        load()
    }

    fun todos() = mainRepository.todos()

    private fun load() = todos().subscribe(object : SingleObserver<List<DataImpl>> {
        override fun onSuccess(t: List<DataImpl>) {
            liveData.setValue(t)
        }

        override fun onSubscribe(d: Disposable) {
        }

        override fun onError(e: Throwable) {
        }
    })
}